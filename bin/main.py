#!/usr/bin/env python
"""
coding=utf-8

Code Template

"""
from __future__  import unicode_literals
import inspect
import logging
import os
import sys
import MySQLdb
from textract import process
import re
import pandas
import spacy

os.chdir("/home/centos/ResumeParser-master/bin/")
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from bin import field_extraction
from bin import lib

def main():
    """
    Main function documentation template
    :return: None
    :rtype: None
    """
    logging.getLogger().setLevel(logging.INFO)
    can_id = sys.argv[1]
    connection = MySQLdb.connect (host = "127.0.0.1" , user = "resume_parser" , passwd = "R()()T$$2o19" , db = "universe_test" )
    cur = connection.cursor()
    q2 = "select candidate_id,resume from universe_candidates where candidate_id = \"{}\"".format(can_id)
    cur.execute(q2)
    d2=cur.fetchall()
    for i in range(0,len(d2)):
      loc = ("../data/input/example_resumes/{}").format(d2[i][1])
      if os.path.exists(loc) == True:
      # Extract data from upstream.
        observations = extract(loc)
        #for i in range(0,len(d2)):
        #f = process(loc)
        #text = re.sub(r'[^\x00-\x7f]',r'', f)
        #print text

      # Spacy: Spacy NLP
        nlp = spacy.load('en_core_web_sm')
        
        #print observations['text'][0]
        #doc = nlp(observations['text'][0].decode('utf8'))
        #doc_entities = doc.ents
        #for ent in doc.ents:
          #print ent.text, ent.label_, ent.vector_norm, ent.is_nered
          #doc_persons = filter(lambda x: x.label_ == 'PERSON', doc_entities)
          #print doc_persons
        #for token in doc:
        #  if (token.text == token.lemma_) and (token.pos_ =='PROPN') and (token.is_alpha == True) and (token.is_stop == False): #and (token.is_title == False):
        #    print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,token.shape_, token.is_alpha, token.is_stop, token.is_oov, token.vector_norm)
               
        
      # Transform data to have appropriate fields
        observations, nlp = transform(observations, nlp)

      # Load data for downstream consumption
        load(observations, nlp, d2[i][0], cur, connection)

        pass
      else:
        print "File already sampled"
    print "Success"
    
def text_extract_utf8(f):
    try:
        f_t = process(f)
        text = re.sub(r'[^\x00-\x7f]',r'', f_t)
        return text
    except UnicodeDecodeError, e:
        return ''     

def extract(location):
    logging.info('Begin extract')

    # Reference variables
    candidate_file_agg = [location]

    # Create list of candidate files
    #for root, subdirs, files in os.walk(lib.get_conf('resume_directory')):
    #    folder_files = map(lambda x: os.path.join(root, x), files)
    #    if location in folder_files:
    #        candidate_file_agg.extend()

    # Convert list to a pandas DataFrame
    observations = pandas.DataFrame(data=candidate_file_agg, columns=['file_path'])
    logging.info('Found {} candidate files'.format(len(observations.index)))

    # Subset candidate files to supported extensions
    observations['extension'] = observations['file_path'].apply(lambda x: os.path.splitext(x)[1])
    observations = observations[observations['extension'].isin(lib.AVAILABLE_EXTENSIONS)]
    logging.info('Subset candidate files to extensions w/ available parsers. {} files remain'.
                 format(len(observations.index)))

    # Attempt to extract text from files
    #print observations['extension'][0]
    #if observations['extension'][0] == ".pdf":
    #  observations['text'] = observations['file_path'].apply(lib.convert_pdf)
    #else:
    #  observations['text'] = observations['file_path'].apply(text_extract_utf8)
    observations['text'] = observations['file_path'].apply(text_extract_utf8)

    # Archive schema and return
    lib.archive_dataset_schemas('extract', locals(), globals())
    logging.info('End extract')
    return observations


def transform(observations, nlp):
    # TODO Docstring
    logging.info('Begin transform')

    # Extract candidate name
    observations['candidate_name'] = observations['text'].apply(lambda x:
                                                                field_extraction.candidate_name_extractor(x, nlp))

    observations['title'] = observations['text'].apply(lambda x:
                                                                field_extraction.candidate_title_extractor(x, nlp, observations['candidate_name'][0]))
    # Extract contact fields
    observations['email'] = observations['text'].apply(lambda x: lib.term_match(x, field_extraction.EMAIL_REGEX))
    observations['phone'] = observations['text'].apply(lambda x: lib.term_match(x, field_extraction.PHONE_REGEX))
    observations['dob'] = observations['text'].apply(lambda x: lib.term_match(x, field_extraction.DOB_REGEX))
    #observations['gender'] = observations['text'].apply(lambda x: lib.term_match(x, field_extraction.GENDER_REGEX))
    # Extract skills
    observations = field_extraction.extract_fields(observations)

    # Archive schema and return
    lib.archive_dataset_schemas('transform', locals(), globals())
    logging.info('End transform')
    return observations, nlp


def load(observations, nlp, cand_id, cur, connection):
    logging.info('Begin load')
    # Open database connection
    final = observations.drop(columns='text')
    for i in range(0, len(final)):
        print final['database'][i],final['programming'][i]
        candidate_name = final['candidate_name'][i].split()
        if len(candidate_name) == 1:
          #candidate_name.extend("")
        #candidate_last_name = final['candidate_name'][i].split()
          basic_info_to_db = "update universe_candidates set first_name = \"{}\", dob = \"{}\", email = \"{}\", contact_number1 = \"{}\", resume_title = \"{}\", primary_skills = \"{}\", secondary_skills = \"{}\", academic = \"{}\" where candidate_id = \"{}\"".format(candidate_name[0],final['dob'][i],final['email'][i],final['phone'][i],final['title'][i],final['programming'][i],final['database'][i],final['universities'][i],cand_id)
        else:
          basic_info_to_db = "update universe_candidates set first_name = \"{}\", last_name = \"{}\", dob = \"{}\", email = \"{}\", contact_number1 = \"{}\", resume_title = \"{}\", primary_skills = \"{}\", secondary_skills = \"{}\", academic = \"{}\" where candidate_id = \"{}\"".format(candidate_name[0],candidate_name[1],final['dob'][i],final['email'][i],final['phone'][i],final['title'][i],final['programming'][i],final['database'][i],final['universities'][i],cand_id)
        #args_b = (cand_id,final['candidate_name'][i],final['email'][i],final['phone'][i],final['title'][i],final['programming'][i],final['database'][i],final['universities'][i])
        cur.execute(basic_info_to_db)
        connection.commit()
        connection.close()
    #    print final['file_path'][i]
    #print final['programming']
    logging.info('End transform')
    #pass


# Main section
if __name__ == '__main__':
    main()
