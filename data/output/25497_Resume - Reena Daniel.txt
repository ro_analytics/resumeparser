  

Reena Daniel 
Strategic HR Business Partner 

 

reenadaniel@hotmail.com +91-9980162332   

 

 

 

 

 

 

Key Skills 

Strategic Planning 

Business Partner 

Policy Formulation  

Performance Management (PMS) 

Learning & Development 

Talent Development 

Change Management 

Team Building & Leadership 

 

 

 

 

 

 

 

 

Education 

 

Profile Summary 

 

 

  A highly accomplished professional with 15 years of rich experience 
in  delivering  sustained  organizational  growth 
in  dynamic 
environments,  establishing  structure,  building  employee  value, 
driving vision and achieving critical strategic goals 

  Played pivotal roles in development of innovative HR initiatives to 
streamline processes, develop employee competence and capitalize 
on organizational growth opportunities 

  Skilled  change  agent,  creative  thinker,  and  decisive  professional 
who effectively balances the needs of employee with the  vision of 
the organization 

  Excellent  track  record  of  implementing  programs  on  continuous 
workforce  development 
for 
improvement  actions,  integrating  workforce  development  with 
process improvement and establishing a culture of excellence: 

setting  priorities 

including 

o  Devised  a  complete  Career  Development  Framework  with 
features such as knowledge development, succession planning, 
competency  development  training  programs  and 
lateral 
movements 

  Strong influencing skills; ability to quickly establish and then maintain 

client and colleague relationships 

  An enterprising leader with expertise in motivating people towards 
achieving organizational objectives while adhering to industry best 
practices 

  MBA in Human Resources from Bharathiar University, Coimbatore in 1997 
  MA in English Literature from Kerala University, Kerala in1995 

Career Timeline 

 
Firstring India Pvt. 
 
 
 

Officer HR 

Ltd., 

First Indian 
Corporation,  

Deputy Manager HR 

WNS Global Services, 
Sr. Group Manager HR 

TeamLease 

Services, 
GM- HRBP 

 

 

 

 

 

 

 

 

 

Nov 00-Sep01 

Feb 04-Feb06 

Jan07-Apr10 

 

 

Apr 10-Jan 12  May14 – June 16 
 

 

June 16- Dec 16  May 17 – Oct 18 

 

 

24/7 Customer Pvt. 

 

Ltd., 

Assistant Manager HR 

 

Aegis Ltd.,           

Site HR Head 

 

Aviators , 
Head HR 

 
 
 
 

  Career Graph 

 

  TeamLease Services Ltd 

 

  Developing People plan for the dedicated business Group based on their business plan and building on the 

principles of the organization’s people strategy 

  Redesigned the Rewards and Recognition Program 
  Led the establishment of a total employee communication system with skip-levels, surveys and open forums 
  Reduction in attrition by 10% through targeted HR interventions 
  HR Integration for newly acquired entities 
  Worked on various interventions to increase employee motivation and efficiency 
  Maintaining  Talent  Pool  by  assessing  and  identifying  critical  positions  and  succession  planning.  Steering 

initiatives for identifying high performing (HI-POS) talent within the organization. 

  Drive and enhance the NEO model. Developed the 30-60-90 day connect with new hires. 
  Conducting  operational  reviews  with  BU  Heads  as  a  health  checkup  for  their  respective  teams  covering 
headcount  details,  attrition  details,  training  status,  employee  development  inputs,  succession  planning, 
actionable from skip level and focus group discussions 
Introduced  the  concept  of  Leadership  Speak  where  Top  Leadership  is  given  an  opportunity  to  share  their 
view/strategy with Hi Potential employees. 
  Handled a team of 6 HRBP across locations 

 

 

 

  WNS Global Services Pvt Ltd 

  Partner and coach line managers in key HR processes (recruitment and selection, performance management, 

talent management) 
Job evaluation of 200 + employees as part of Competency framework creation 

 
  Drove  initiatives  such  as  cross  skilling  at  middle  management  level  which  resulted  in  maximizing  human 

potential and supported internal growth 

  Talent Management by Grooming sessions, FLM Effectiveness, Mentoring initiatives, Redeployment process e 
  Devised Employee Study programs 
  Reduction in attrition by 5% through targeted HR interventions 
  Managed a team of 4 senior HRBPs 
 

 

  Aegis Ltd 

  Managed the complete recruitment life-cycle for sourcing the best talent from diverse sources  
  Partner and coach line managers in key HR processes (recruitment and selection, performance management, 

talent management) 

  Provides day-to-day performance management guidance to line management (coaching, counselling, performance reviews, 

career development and disciplinary actions) 

  Participate in any cross-divisional HR projects and the design and implementation of new HR policies and practices 
  Managed a team of 14 HR members across TA, HR Operations, Employee Relations 

  First Indian Corporation 

  Was instrumental in setting up the Employee Relations team for the organisation of 5000 employees across 3 

locations 

  Created  HR  Policies  and  Employee  Handbook  including  revision  and  implementation  of  policies  and 

procedures ensuring compliance with various regulations 

  Designed and implemented the Assessment centre for promotions along with the L&D team 
  Managed a team of 5 HR members across locations 

 

 

 

 

 

 
 
 
 

 

  Other Notable Accomplishments Across the Career 

 

  Responsible  for  development,  implementation,  administration  and  operation  of  high-value  quality  human 

 

 

resources programs, practices and procedures to all levels of team members 
Integration of complex business factors to build business capability, develop leaders and succession plan, and 
drive business change efforts 
Introduced New ON Boarding and Induction plan to create revitalized Employee Experience by  introducing 
Welcome Call, Welcome e-Mail and post Hire survey which help in capturing New Hire experience and provide 
realistic view on Employee Experience as a part of retention Strategy 

  Analyzes business challenges and HR programs to identify improvement opportunities 
  Developing people plan for the dedicated business Group based on their business plan and building on the 

principles of the organization’s people strategy 

  Developed and implemented innovative retention strategies 
  Performing research on strategic executive succession planning, human capital infrastructure, retention and 

 

knowledge management projects and making recommendations to senior management 
Instituted  change-management  practices  in  the  organization,  including  organizational  restructuring  & 
manpower rationalization initiatives 

  Devising  and  developing  evaluation  criteria  for  all  business  verticals  in  coordination  with  Business  Heads; 

establishing SMART goals for PMS 

  Planned & implemented functional reviews for various departments as a part of Talent Management Process 

to enhance employee engagement and morale 

  Actively counsel and advice on multiple initiatives, recommend solutions to complex employee issues.  
  Formulating  cost-effective  training  budget,  sourcing  and  appointing trainers &  agencies,  arranging  various 

behavioural and technical training programs 

  Anchored Employee Assistance Program 
  Performing  regular  dipsticks  to  gauge  the  pulse  from  the  grass  root  level  and  formulated  action  plans  to 

address concern areas 

Certifications | Trainings | Workshop 

  Certified Internal Process Auditor 
  Certified Performance & Competency Developer 
  Leadership Skills Enhancement Program 
  Women in Leadership by NASSCOM 
  Virtual Business Simulation Workshop 

 

 
 

 

